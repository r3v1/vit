import numpy as np
import torch


def positional_embedding(sequence_length: int, d: int) -> torch.Tensor:
    """
    Positional encoding allows the model to understand where each patch would be
    placed in the original image. In particular, positional encoding adds low-frequency
    values to the first dimensions and higher-frequency values to the latter dimensions.

    Returns
    -------

    References
    ----------
    - https://miro.medium.com/max/1400/1*lpRYHE0XjVkxRVKFrWkzuw.png

    """
    result = np.ones((sequence_length, d))
    for i in range(sequence_length):
        for j in range(d):
            if j % 2 == 0:
                result[i][j] = np.sin(i / (10000 ** (j / d)))
            else:
                result[i][j] = np.cos(i / (10000 ** ((j - 1) / d)))
    
    return torch.from_numpy(result)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    x = positional_embedding(100, 300)
    plt.imshow(x, cmap="hot", interpolation="nearest")
    plt.tight_layout()
    plt.show()
