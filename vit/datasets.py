from torch.utils.data import DataLoader
from torchvision.datasets.mnist import MNIST
from torchvision.transforms import ToTensor


def load_mnist(batch_size: int = 32):
    X_train = MNIST(root="../datasets", train=True, download=True, transform=ToTensor())
    # X_train.train_data.to("cuda")
    # X_train.train_labels.to("cuda")

    X_test = MNIST(root="../datasets", train=False, download=True, transform=ToTensor())
    # X_test.train_data.to("cuda")
    # X_test.train_labels.to("cuda")
    
    train_loader = DataLoader(X_train, shuffle=True, batch_size=batch_size, )
    test_loader = DataLoader(X_test, shuffle=False, batch_size=batch_size, )
    
    return train_loader, test_loader


if __name__ == "__main__":
    load_mnist()
