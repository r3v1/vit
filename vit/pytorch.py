"""
https://medium.com/mlearning-ai/vision-transformers-from-scratch-pytorch-a-step-by-step-guide-96c3313c2e0c
https://towardsdatascience.com/understand-and-implement-vision-transformer-with-tensorflow-2-0-f5435769093
https://arxiv.org/abs/2106.10270
"""

from pathlib import Path

import numpy as np
import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss
from torch.optim import Adam
from torch.utils.data import DataLoader
from tqdm import tqdm

from vit.datasets import load_mnist
from vit.utils import positional_embedding

np.random.seed(0)
torch.manual_seed(0)


class MultiHeadAttention(nn.Module):
    """
    Multi-Head Attention module

    Multi-head attention mechanisms are crucial components of Transformer model.

    Multi-head attention mechanism
    ------------------------------
    In order to calculate attentions with a "query", you compare the 'query' with
    the 'keys' and get scores/weights for the 'values'. Each score/weight is in
    short the relevance between the 'query' and each 'key'. And you reweight the
    'values' with the scores/weights, and take the summation of the reweighted 'values'.

    References
    ----------
    - https://arxiv.org/abs/1706.03762
    - https://data-science-blog.com/blog/2021/04/07/multi-head-attention-mechanism/
    """

    def __init__(self, d: int, n_heads: int):
        super(MultiHeadAttention, self).__init__()
        self.d = d
        self.n_heads = n_heads

        assert d % n_heads == 0, f"{d} must be divisible by {n_heads}"

        d_head = int(d / n_heads)
        self.q_mappings = [nn.Linear(d_head, d_head) for _ in range(n_heads)]
        self.k_mappings = [nn.Linear(d_head, d_head) for _ in range(n_heads)]
        self.v_mappings = [nn.Linear(d_head, d_head) for _ in range(n_heads)]

        self.d_head = d_head
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, sequences: torch.Tensor):
        result = []
        for sequence in sequences:
            seq_result = []
            for head in range(self.n_heads):
                q_mapping = self.q_mappings[head]
                k_mapping = self.k_mappings[head]
                v_mapping = self.v_mappings[head]

                seq = sequence[:, head * self.d_head: (head + 1) * self.d_head]
                q, k, v = q_mapping(seq), k_mapping(seq), v_mapping(seq)

                attention = self.softmax(q @ k.T / (self.d_head ** 0.5))
                seq_result.append(attention @ v)

            result.append(torch.cat(seq_result, dim=-1))

        return torch.cat([torch.unsqueeze(r, dim=0) for r in result])


class ViT(nn.Module):
    """
    1. Input image is "cut" into sub-images
    2. Each image is then linearly embedded (then, each image is just a vector)
    3. A positional embedding is added to these vectors
    4. These tokens are then passed to the transformer encoder
        4.1 Norm layer
        4.2 Multi Head attention
        4.3 Residual connections
        4.4 Norm layer
        4.5 MLP (Classification)
    """

    def __init__(self,
                 input_shape: tuple,
                 n_patches: int = 7,
                 hidden_d: int = 8,
                 n_heads: int = 2,
                 out_d: int = 10):
        super(ViT, self).__init__()

        # Check image is divisible entirely by the number of patches
        assert input_shape[0] % n_patches == 0
        assert input_shape[1] % n_patches == 0

        self.input_shape = input_shape  # Input shape (batch, width, height, channel)
        self.n_patches = n_patches  # Break the image in n_patches x n_patches
        self.patch_size = (input_shape[0] / n_patches, input_shape[1] / n_patches)
        self.input_d = int(input_shape[2] * self.patch_size[0] * self.patch_size[1])

        # While each patch was a 4x4=16 dimensional vector, the linear mapping can map to any arbitrary vector size
        self.hidden_d = hidden_d

        # Linear embedding
        self.linear_mapper = nn.Linear(self.input_d, self.hidden_d)

        # Classification token
        self.class_token = nn.Parameter(torch.rand(1, self.hidden_d))

        # 4.1) Layer Normalization
        # Layer normalization is a popular block that, given an input, subtracts its
        # mean and divides by the standard deviation.
        self.ln1 = nn.LayerNorm((self.n_patches ** 2 + 1, self.hidden_d))

        # 4.2) Multi-head Self Attention
        # We now need to implement sub-figure c of the architecture picture. We
        # want, for a single image, that each patch gets updated based on some similarity
        # measure with the other patches. We do so by linearly mapping each patch (that
        # is now an 8-dimensional vector in our example) to 3 distinct vectors: q, k, and v
        # (query, key, value).
        self.mha = MultiHeadAttention(self.hidden_d, n_heads=n_heads)
        # self.mha = nn.MultiheadAttention(self.hidden_d, n_heads)

        # 5a) Layer Normalization 2
        self.ln2 = nn.LayerNorm((self.n_patches ** 2 + 1, self.hidden_d))

        # 5b) Encoder MLP
        self.enc_mlp = nn.Sequential(
            nn.Linear(self.hidden_d, self.hidden_d),
            nn.ReLU(),
        )

        # 6) Classification MLP
        self.mlp = nn.Sequential(
            nn.Linear(self.hidden_d, out_d),
            nn.Softmax(dim=-1),
        )

    def _extract_patches(self, x):
        """
        Dadas n imágenes (n, width, height, channel), extrae sub-imágenes
        de tamaño self.patch_size, pero aplándolas de tal que forma que una
        imagen (width, height, channel) -> (*self.patch_size, self.input_d)


        Parameters
        ----------
        x: torch.Tensor

        Returns
        -------
        torch.Tensor

        Examples
        --------
        >>> x = torch.rand(3, 28, 28, 3)
        >>> model = ViT(input_shape=(28, 28, 3), n_patches=7)
        >>> model._extract_patches(x).shape  # (3, 7 ** 2, 28 // 7 * 28//7 * 3)
        torch.Size([3, 49, 48])
        """
        n, w, h, c = x.shape
        patches = x.reshape(n, self.n_patches ** 2, self.input_d)  # Pasan a ser vectores

        return n, patches

    def forward(self, images):
        # Dividing images into patches
        n, patches = self._extract_patches(images)

        # 1) Linear embedding
        tokens = self.linear_mapper(patches)

        # 2) Adding classification token to the tokens
        tokens = torch.stack([
            torch.vstack((self.class_token, tokens[i])) for i in range(len(tokens))
        ])

        # 3) Adding positional embedding
        tokens += positional_embedding(self.n_patches ** 2 + 1, self.hidden_d).repeat(n, 1, 1)

        # TRANSFORMER ENCODER BEGINS
        # 4) LayerNormalization, Multi-head Self Attention and Residual Connection
        out = tokens + self.mha(self.ln1(tokens))

        # 5) Run Layer Normalization, MLP and Residual Connection
        out = out + self.enc_mlp(self.ln2(out))
        # TRANSFORMER ENCODER ENDS

        # 6) Getting the classification token only
        out = out[:, 0]

        return self.mlp(out)


def main(train_loader: DataLoader, epochs: int = 5, lr: float = 0.01, from_checkpoint: str = None):
    model = ViT(input_shape=(28, 28, 1), n_patches=7, hidden_d=8, n_heads=4, out_d=10)

    # Training loop
    optimizer = Adam(model.parameters(), lr=lr)
    criterion = CrossEntropyLoss()
    ckpt_train_loss = None

    from_checkpoint = Path(from_checkpoint).expanduser()
    if from_checkpoint.is_file():
        print(f"Cargando checkpoint")

        checkpoint = torch.load(from_checkpoint)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        ckpt_train_loss = checkpoint['loss']
        model.train()

        print(f"Last train_loss: {ckpt_train_loss:.2f}")

    best_train_loss = torch.inf
    try:
        for epoch in range(epochs):
            train_loss = 0.0
            t_ = tqdm(train_loader)

            for batch in t_:
                x, y = batch

                # Reshape to channel last
                n, c, w, h = x.shape
                x = x.view(n, w, h, c)

                y_hat = model(x)  # Predict
                loss = criterion(y_hat, y) / len(x)

                train_loss = loss.item()

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                t_.set_description(f"Epoch {epoch + 1} / {epochs} - Train loss {train_loss:.4f}")

            if ckpt_train_loss is None or (train_loss < ckpt_train_loss and best_train_loss > train_loss):
                best_train_loss = train_loss
                torch.save({
                    'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': train_loss,
                }, f"model_{epoch:03}.ckpt")

    except KeyboardInterrupt:
        print(f"Parando... Best train loss {best_train_loss:.2f}")

    return model


def run_test(test_loader: DataLoader, path: str):
    model = ViT(input_shape=(28, 28, 1), n_patches=7, hidden_d=8, n_heads=4, out_d=10)

    # Load checkpoint
    checkpoint = torch.load(path)
    model.load_state_dict(checkpoint['model_state_dict'])

    # Load saved model
    # model.load_state_dict(torch.load("model.pt"))

    model.eval()  # Set dropout and batch normalization layers to evaluation mode

    # Test loop
    criterion = CrossEntropyLoss()
    correct, total = 0, 0
    test_loss = 0.0

    for batch in test_loader:
        x, y = batch
        y_hat = model(x)  # Predict
        loss = criterion(y_hat, y)
        test_loss = loss / len(x)

        correct += torch.sum(torch.argmax(y_hat, dim=1) == y).item()
        total += len(x)

    print(f"Test loss: {test_loss:.4f}")
    print(f"Accuracy: {correct / total * 100:.2f}")


if __name__ == "__main__":
    # print(f"Allocated {torch.cuda.memory_allocated() / 1024 ** 2:.2f}MB")
    # print(f"Reserved {torch.cuda.memory_reserved() / 1024 ** 2:.2f}MB")

    train_loader, test_loader = load_mnist()

    main(train_loader, epochs=5, lr=0.01, from_checkpoint="model_001.ckpt")

    # Test
    run_test(test_loader, path="model_003.ckpt")

    # x = torch.rand(3, 28, 28, 1)
    # model = ViT(input_shape=(28, 28, 1), n_patches=7, hidden_d=8, n_heads=2, out_d=10)
    # y = model(x)
    # print(y.shape)
