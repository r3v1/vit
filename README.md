# Vision Transformers from scratch 

- Original paper: [ArXiV](https://arxiv.org/pdf/2010.11929.pdf)
- How to train your ViT? ... : [ArXiV](https://arxiv.org/pdf/2106.10270.pdf)
- Yannic Kilcher's video: [YouTube](https://www.youtube.com/watch?v=TrdevFK_am4)

## [PyTorch](https://medium.com/mlearning-ai/vision-transformers-from-scratch-pytorch-a-step-by-step-guide-96c3313c2e0c)

TODO: Step 2

## [Tensorflow](https://towardsdatascience.com/understand-and-implement-vision-transformer-with-tensorflow-2-0-f5435769093)

### Resources

- [extract_patches](https://www.tensorflow.org/api_docs/python/tf/image/extract_patches)